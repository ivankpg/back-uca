<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\Models\Persona::class, function (Faker\Generator $faker) {

    return [
        'documento' => $faker->unique()->numberBetween($min=10000000, $max=99999999),
        'nombre' => $faker->firstName,
        'apellido' => $faker->lastName
    ];
});
