<?php

use Illuminate\Database\Seeder;

class ConfiguracionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('es_ar');
        for ($i=0; $i < 15; $i++) {
            DB::table('materias')->insert([
                'codigo' => $faker->unique()->numberBetween(1000,9999),
                'nombre' => 'Materia' . ' ' . $i,
            ]);
        }

        for ($i=0; $i < 5; $i++) {
            DB::table('carreras')->insert([
                'codigo' => $faker->unique()->numberBetween(01,10),
                'nombre' => 'Carrera' . ' ' . $i,
            ]);
        }

        for ($i=0; $i < 2; $i++) {
            DB::table('facultades')->insert([
                'nombre' => 'Facultad' . ' ' . $i,
            ]);
        }
    }
}
