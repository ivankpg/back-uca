<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfiguracionSeeder::class);
        $this->call(AlumnosTableSeeder::class);
        $this->call(ComisionSeeder::class);
        $this->call(InscripcionesSeeder::class);
    }
}
