<?php

use App\Models\Alumno;
use App\Models\Comision;
use App\Models\Inscripcion;
use Illuminate\Database\Seeder;

class InscripcionesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alumnos = Alumno::all();
        foreach ($alumnos as $a){
            /** @var \App\Models\AlumnoCarrera $alumnoCarrera */
            foreach ($a->alumnoCarreras as $alumnoCarrera){
                $carreraAlumno = $alumnoCarrera->carrera->id;
                $comision = Comision::where('id_carrera', $carreraAlumno)->first();
                $inscripcion = new Inscripcion;
                $inscripcion->id_alumno = $a->id;
                $inscripcion->id_comision = $comision->id;
                $inscripcion->save();
            }

        }
    }
}
