<?php

use App\Models\Alumno;
use App\Models\AlumnoCarrera;
use App\Models\Carrera;
use App\Models\Persona;
use Illuminate\Database\Seeder;

class AlumnosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('es_ar');
        for ($i=0; $i < 150; $i++) {

            $persona = new Persona;

            $persona->documento = $faker->unique()->numberBetween(10000000,99999999);
            $persona->nombre = $faker->firstName;
            $persona->apellido = $faker->lastName;
            $persona->save();

            $alumno = new Alumno;

            $alumno->legajo = $faker->unique()->numberBetween(1000000,9999999);
            $alumno->id_persona = $persona->id;
            $alumno->save();
        }

        $alumnos = Alumno::all();
        foreach ($alumnos as $a){
            $carrera = Carrera::inRandomOrder()->first();

            $alumnoCarrera = new AlumnoCarrera;
            $alumnoCarrera->id_alumno = $a->id;
            $alumnoCarrera->id_carrera = $carrera->id;
            $alumnoCarrera->regular = $faker->boolean;
            $alumnoCarrera->save();
        }

    }

}
