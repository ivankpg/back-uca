<?php

use App\Models\Carrera;
use App\Models\Comision;
use App\Models\Facultad;
use App\Models\Materia;
use Illuminate\Database\Seeder;

class ComisionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('es_ar');
        for ($i=0; $i < 30; $i++) {
            $facultad = Facultad::inRandomOrder()->first();
            $carrera = Carrera::inRandomOrder()->first();
            $materia = Materia::inRandomOrder()->first();

            $comision = new Comision;

            $comision->id_facultad = $facultad->id;
            $comision->id_carrera = $carrera->id;
            $comision->id_materia = $materia->id;
            $comision->catedra = $faker->regexify('[A-Z]');
            $comision->turno = $faker->randomElement($array = array ('M','T','N'));
            $comision->semestre = $faker->randomElement($array = array ('1','2'));
            $comision->anio = $faker->numberBetween(2012,2018);

            $comision->save();
        }
    }
}
