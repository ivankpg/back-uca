<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComisionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comisiones', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_facultad')->unsigned();
            $table->integer('id_carrera')->unsigned();
            $table->integer('id_materia')->unsigned();
            $table->char('catedra');
            $table->char('turno');
            $table->char('semestre');
            $table->string('anio');
            $table->timestamps();

            $table->foreign('id_facultad')->references('id')->on('facultades');
            $table->foreign('id_carrera')->references('id')->on('carreras');
            $table->foreign('id_materia')->references('id')->on('materias');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comisiones');
    }
}
