<?php

// get listado de comisiones
Route::get('comisiones','ComisionController@getComisiones');
// get comision por id
Route::get('comisiones/{id}','ComisionController@getListaAlumnosPorComision');
// get alumnos de una comision por id de la comision
Route::get('comisiones/{id}/alumnos','ComisionController@getListaAlumnosPorComision');

// get listado de carreras
Route::get('carreras','ConfiguracionController@getCarreras');
// get listado de materias
Route::get('materias','ConfiguracionController@getMaterias');
// get listado de facultades
Route::get('facultades','ConfiguracionController@getFacultades');

Route::post('actas','ActaController@store');
