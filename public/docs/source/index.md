---
title: API Reference

language_tabs:
- bash
- javascript

includes:

search: true

toc_footers:
- <a href='http://github.com/mpociot/documentarian'>Documentation Powered by Documentarian</a>
---
<!-- START_INFO -->
# Info

Welcome to the generated API reference.
[Get Postman Collection](http://localhost/docs/collection.json)

<!-- END_INFO -->

#general
<!-- START_878057588d5c5c517a27d2965b5ba4ae -->
## Listado de comisiones con filtros por año, turno, materia, catedra, semestre y carrera.

> Example request:

```bash
curl -X GET -G "http://localhost/api/comisiones" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/comisiones",
    "method": "GET",
    "headers": {
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "message": "Undefined index: anio",
    "exception": "ErrorException",
    "file": "C:\\xampp\\htdocs\\back-uca\\app\\Http\\Controllers\\ComisionController.php",
    "line": 24,
    "trace": [
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\app\\Http\\Controllers\\ComisionController.php",
            "line": 24,
            "function": "handleError",
            "class": "Illuminate\\Foundation\\Bootstrap\\HandleExceptions",
            "type": "->"
        },
        {
            "function": "getComisiones",
            "class": "App\\Http\\Controllers\\ComisionController",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Controller.php",
            "line": 54,
            "function": "call_user_func_array"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\ControllerDispatcher.php",
            "line": 45,
            "function": "callAction",
            "class": "Illuminate\\Routing\\Controller",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php",
            "line": 212,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\ControllerDispatcher",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Route.php",
            "line": 169,
            "function": "runController",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 658,
            "function": "run",
            "class": "Illuminate\\Routing\\Route",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\SubstituteBindings.php",
            "line": 41,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 149,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\SubstituteBindings",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Middleware\\ThrottleRequests.php",
            "line": 57,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 149,
            "function": "handle",
            "class": "Illuminate\\Routing\\Middleware\\ThrottleRequests",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 102,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 660,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 635,
            "function": "runRouteWithinStack",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 601,
            "function": "runRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Router.php",
            "line": 590,
            "function": "dispatchToRoute",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 176,
            "function": "dispatch",
            "class": "Illuminate\\Routing\\Router",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 30,
            "function": "Illuminate\\Foundation\\Http\\{closure}",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 149,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest.php",
            "line": 30,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 149,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\TransformsRequest",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize.php",
            "line": 27,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 149,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\ValidatePostSize",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode.php",
            "line": 46,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 149,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Middleware\\CheckForMaintenanceMode",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Routing\\Pipeline.php",
            "line": 53,
            "function": "Illuminate\\Pipeline\\{closure}",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Pipeline\\Pipeline.php",
            "line": 102,
            "function": "Illuminate\\Routing\\{closure}",
            "class": "Illuminate\\Routing\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 151,
            "function": "then",
            "class": "Illuminate\\Pipeline\\Pipeline",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Http\\Kernel.php",
            "line": 116,
            "function": "sendRequestThroughRouter",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Tools\\ResponseStrategies\\ResponseCallStrategy.php",
            "line": 199,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Http\\Kernel",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Tools\\ResponseStrategies\\ResponseCallStrategy.php",
            "line": 185,
            "function": "callLaravelRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Tools\\ResponseStrategies\\ResponseCallStrategy.php",
            "line": 25,
            "function": "makeApiCall",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Tools\\ResponseResolver.php",
            "line": 33,
            "function": "__invoke",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseStrategies\\ResponseCallStrategy",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Tools\\ResponseResolver.php",
            "line": 42,
            "function": "resolve",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Tools\\Generator.php",
            "line": 54,
            "function": "getResponse",
            "class": "Mpociot\\ApiDoc\\Tools\\ResponseResolver",
            "type": "::"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php",
            "line": 194,
            "function": "processRoute",
            "class": "Mpociot\\ApiDoc\\Tools\\Generator",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\mpociot\\laravel-apidoc-generator\\src\\Commands\\GenerateDocumentation.php",
            "line": 56,
            "function": "processRoutes",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "function": "handle",
            "class": "Mpociot\\ApiDoc\\Commands\\GenerateDocumentation",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 29,
            "function": "call_user_func_array"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 87,
            "function": "Illuminate\\Container\\{closure}",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\BoundMethod.php",
            "line": 31,
            "function": "callBoundMethod",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Container\\Container.php",
            "line": 549,
            "function": "call",
            "class": "Illuminate\\Container\\BoundMethod",
            "type": "::"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 183,
            "function": "call",
            "class": "Illuminate\\Container\\Container",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\symfony\\console\\Command\\Command.php",
            "line": 255,
            "function": "execute",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Command.php",
            "line": 170,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Command\\Command",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\symfony\\console\\Application.php",
            "line": 946,
            "function": "run",
            "class": "Illuminate\\Console\\Command",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\symfony\\console\\Application.php",
            "line": 248,
            "function": "doRunCommand",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\symfony\\console\\Application.php",
            "line": 148,
            "function": "doRun",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Console\\Application.php",
            "line": 88,
            "function": "run",
            "class": "Symfony\\Component\\Console\\Application",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\vendor\\laravel\\framework\\src\\Illuminate\\Foundation\\Console\\Kernel.php",
            "line": 121,
            "function": "run",
            "class": "Illuminate\\Console\\Application",
            "type": "->"
        },
        {
            "file": "C:\\xampp\\htdocs\\back-uca\\artisan",
            "line": 35,
            "function": "handle",
            "class": "Illuminate\\Foundation\\Console\\Kernel",
            "type": "->"
        }
    ]
}
```

### HTTP Request
`GET api/comisiones`


<!-- END_878057588d5c5c517a27d2965b5ba4ae -->

<!-- START_00611d80707b39b2891d9a8db5794540 -->
## Listado de alumnos de una comisión

> Example request:

```bash
curl -X GET -G "http://localhost/api/comisiones/{id}" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/comisiones/{id}",
    "method": "GET",
    "headers": {
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "data": [
        {
            "alumnoId": 3,
            "nombreAlumno": "Abigail Franco",
            "legajo": 1038102,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "5",
            "tieneNota": true
        },
        {
            "alumnoId": 5,
            "nombreAlumno": "Amelia Guerrero",
            "legajo": 4855382,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 10,
            "nombreAlumno": "Josefina Olivo",
            "legajo": 2083578,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 13,
            "nombreAlumno": "Ximena Collazo",
            "legajo": 7947063,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 15,
            "nombreAlumno": "Joaquín Castellanos",
            "legajo": 2995528,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 18,
            "nombreAlumno": "Vicente León",
            "legajo": 1920874,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "4",
            "tieneNota": true
        },
        {
            "alumnoId": 28,
            "nombreAlumno": "Maximiliano Alarcón",
            "legajo": 9391500,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 29,
            "nombreAlumno": "Sara Hidalgo",
            "legajo": 7394622,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 35,
            "nombreAlumno": "Alejandra Gamboa",
            "legajo": 6773502,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 40,
            "nombreAlumno": "Mateo Ureña",
            "legajo": 1444096,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 44,
            "nombreAlumno": "Benjamín Castillo",
            "legajo": 2643889,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        }
    ]
}
```

### HTTP Request
`GET api/comisiones/{id}`


<!-- END_00611d80707b39b2891d9a8db5794540 -->

<!-- START_df2a7a15a1323c82ef3ef7eab0898460 -->
## Listado de alumnos de una comisión

> Example request:

```bash
curl -X GET -G "http://localhost/api/comisiones/{id}/alumnos" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/comisiones/{id}/alumnos",
    "method": "GET",
    "headers": {
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "data": [
        {
            "alumnoId": 3,
            "nombreAlumno": "Abigail Franco",
            "legajo": 1038102,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "5",
            "tieneNota": true
        },
        {
            "alumnoId": 5,
            "nombreAlumno": "Amelia Guerrero",
            "legajo": 4855382,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 10,
            "nombreAlumno": "Josefina Olivo",
            "legajo": 2083578,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 13,
            "nombreAlumno": "Ximena Collazo",
            "legajo": 7947063,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 15,
            "nombreAlumno": "Joaquín Castellanos",
            "legajo": 2995528,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 18,
            "nombreAlumno": "Vicente León",
            "legajo": 1920874,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "4",
            "tieneNota": true
        },
        {
            "alumnoId": 28,
            "nombreAlumno": "Maximiliano Alarcón",
            "legajo": 9391500,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 29,
            "nombreAlumno": "Sara Hidalgo",
            "legajo": 7394622,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 35,
            "nombreAlumno": "Alejandra Gamboa",
            "legajo": 6773502,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 40,
            "nombreAlumno": "Mateo Ureña",
            "legajo": 1444096,
            "regular": false,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        },
        {
            "alumnoId": 44,
            "nombreAlumno": "Benjamín Castillo",
            "legajo": 2643889,
            "regular": true,
            "carreraNombre": "Carrera 1",
            "carreraId": 2,
            "carreraCodigo": "6",
            "nota": "",
            "tieneNota": false
        }
    ]
}
```

### HTTP Request
`GET api/comisiones/{id}/alumnos`


<!-- END_df2a7a15a1323c82ef3ef7eab0898460 -->

<!-- START_64f519dfcb5ce084204b48ecb82f8168 -->
## Listado de carreras

> Example request:

```bash
curl -X GET -G "http://localhost/api/carreras" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/carreras",
    "method": "GET",
    "headers": {
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "data": [
        {
            "codigo": "Carrera 0",
            "nombre": "7"
        },
        {
            "codigo": "Carrera 1",
            "nombre": "6"
        },
        {
            "codigo": "Carrera 2",
            "nombre": "5"
        },
        {
            "codigo": "Carrera 3",
            "nombre": "10"
        },
        {
            "codigo": "Carrera 4",
            "nombre": "9"
        }
    ]
}
```

### HTTP Request
`GET api/carreras`


<!-- END_64f519dfcb5ce084204b48ecb82f8168 -->

<!-- START_3bdf1181dd4a04b3f7453492938252c9 -->
## Listado de materias

> Example request:

```bash
curl -X GET -G "http://localhost/api/materias" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/materias",
    "method": "GET",
    "headers": {
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "data": [
        {
            "codigo": "Materia 0",
            "nombre": "5078"
        },
        {
            "codigo": "Materia 1",
            "nombre": "4581"
        },
        {
            "codigo": "Materia 2",
            "nombre": "6069"
        },
        {
            "codigo": "Materia 3",
            "nombre": "3828"
        },
        {
            "codigo": "Materia 4",
            "nombre": "6420"
        },
        {
            "codigo": "Materia 5",
            "nombre": "4434"
        },
        {
            "codigo": "Materia 6",
            "nombre": "8647"
        },
        {
            "codigo": "Materia 7",
            "nombre": "7347"
        },
        {
            "codigo": "Materia 8",
            "nombre": "8085"
        },
        {
            "codigo": "Materia 9",
            "nombre": "2345"
        },
        {
            "codigo": "Materia 10",
            "nombre": "4063"
        },
        {
            "codigo": "Materia 11",
            "nombre": "3296"
        },
        {
            "codigo": "Materia 12",
            "nombre": "9581"
        },
        {
            "codigo": "Materia 13",
            "nombre": "2632"
        },
        {
            "codigo": "Materia 14",
            "nombre": "2908"
        }
    ]
}
```

### HTTP Request
`GET api/materias`


<!-- END_3bdf1181dd4a04b3f7453492938252c9 -->

<!-- START_a456a46df9160963422218f73684ff7e -->
## Listado de facultades

> Example request:

```bash
curl -X GET -G "http://localhost/api/facultades" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/facultades",
    "method": "GET",
    "headers": {
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "data": [
        {
            "codigo": "Facultad 0"
        },
        {
            "codigo": "Facultad 1"
        }
    ]
}
```

### HTTP Request
`GET api/facultades`


<!-- END_a456a46df9160963422218f73684ff7e -->

<!-- START_b8e0afeee283bb18a308da5409c46a36 -->
## Guarda las actas con las notas de los alumnos que han sido enviadas.

> Example request:

```bash
curl -X POST "http://localhost/api/actas" 
```

```javascript
var settings = {
    "async": true,
    "crossDomain": true,
    "url": "http://localhost/api/actas",
    "method": "POST",
    "headers": {
    }
}

$.ajax(settings).done(function (response) {
    console.log(response);
});
```

> Example response:

```json
{
    "success": true,
    "mensaje": "Se han guardado las notas."
}
```

### HTTP Request
`POST api/actas`


<!-- END_b8e0afeee283bb18a308da5409c46a36 -->


