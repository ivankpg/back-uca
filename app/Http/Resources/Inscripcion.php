<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Inscripcion extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        {

            return [
                'alumnoId' => $this->alumnoCarrera->alumno->id,
                'nombreAlumno' => $this->alumnoCarrera->alumno->persona->getFullName(),
                'legajo' => $this->alumnoCarrera->alumno->legajo,
                'regular' => (boolean)$this->alumnoCarrera->regular,
                'carreraNombre' => $this->alumnoCarrera->carrera->nombre,
                'carreraId' => $this->alumnoCarrera->carrera->id,
                'carreraCodigo' => $this->alumnoCarrera->carrera->codigo,
                'nota' => $this->acta ? $this->acta->nota : '',
                'tieneNota' => $this->acta ? true : false
            ];
        }
    }
}
