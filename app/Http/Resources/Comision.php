<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class Comision extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'facultad' => $this->facultad->nombre,
            'carrera' => $this->carrera->nombre,
            'materiaCodigo' => $this->materia->codigo,
            'materiaNombre' => $this->materia->nombre,
            'catedra' => $this->catedra,
            'turno' => $this->turno,
            'semestre' => $this->semestre,
            'anio' => $this->anio
        ];
    }
}
