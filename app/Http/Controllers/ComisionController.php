<?php

namespace App\Http\Controllers;

use App\Models\Acta;
use App\Models\Comision;
use App\Models\Inscripcion;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use App\Http\Resources\Comision as ComisionResource;
use App\Http\Resources\Inscripcion as InscripcionResource;
use Illuminate\Support\Facades\DB;

class ComisionController extends Controller
{

    /**
     * Listado de comisiones con filtros por año, turno, materia, catedra, semestre y carrera.
     * @param Request $request
     * @return array
     */
    public function getComisiones(Request $request)
    {
        $anio = $request->all()['anio'];
        $turno = $request->all()['turno'];
        $materia = $request->all()['materia'];
        $catedra = $request->all()['catedra'];
        $semestre = $request->all()['semestre'];
        $carrera = $request->all()['carrera'];

        $comisiones = DB::table('comisiones as c')
            ->join('materias as m', 'c.id_materia','=','m.id')
            ->join('carreras as ca', 'c.id_carrera','=','ca.id')
            ->join('facultades as f', 'c.id_facultad','=','f.id')
            ->select('c.*',
                'ca.nombre as nombreCarrera',
                'm.nombre as nombreMateria', 'm.codigo as codigoMateria',
                'f.nombre as nombreFacultad')
            ->when($anio, function ($query) use ($anio) {
                return $query->where('c.anio', $anio);
            })
            ->when($turno, function ($query) use ($turno) {
                return $query->where('turno', $turno);
            })
            ->when($materia, function ($query) use ($materia) {
                return $query->where('m.nombre','like', '%' .$materia . '%');
            })
            ->when($catedra, function ($query) use ($catedra) {
                return $query->where('catedra', $catedra);
            })
            ->when($semestre, function ($query) use ($semestre) {
                return $query->where('semestre', $semestre);
            })
            ->when($carrera, function ($query) use ($carrera) {
                return $query->where('ca.nombre', 'like', '%' .$carrera. '%');
            })
            ->orderBy('c.id')
            ->get();

        $comisionesCollection = new Collection();
        foreach ($comisiones as $c){
            $alumnos = Comision::findOrfail(intval($c->id))->inscripciones->count();;

            $comision = [
                'id' => $c->id,
                'facultad' => $c->nombreFacultad,
                'carrera' => $c->nombreCarrera,
                'materiaCodigo' => $c->codigoMateria,
                'materiaNombre' => $c->nombreMateria,
                'catedra' => $c->catedra,
                'turno' => $c->turno,
                'semestre' => $c->semestre,
                'anio' => $c->anio,
                'cantidadAlumnos' => $alumnos
            ];
            $comisionesCollection->add($comision);
        }

        return [
            'data' => $comisionesCollection
        ];
    }

    /**
     * Detalle de una comisión
     * @param $id
     * @return ComisionResource
     */
    public function show($id)
    {
        $comision = Comision::findOrfail($id);
        return new ComisionResource($comision);

    }


    /**
     * Listado de alumnos de una comisión
     * @param $id
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getListaAlumnosPorComision($id){
        $comision = Comision::findOrfail($id);
        $inscripciones = Inscripcion::where('id_comision',$comision->id)->get();
        return InscripcionResource::collection($inscripciones);
    }
}
