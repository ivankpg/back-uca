<?php

namespace App\Http\Controllers;

use App\Models\Acta;
use App\Models\Alumno;
use App\Models\Comision;
use App\Models\Inscripcion;
use Illuminate\Http\Request;
use App\Http\Resources\Acta as ActaResource;
use Exception;
class ActaController extends Controller
{

    /**
     * Guarda las actas con las notas de los alumnos que han sido enviadas.
     * @param Request $request
     * @return array
     */
    public function store(Request $request)  {

        $actas = $request->all();
        foreach ($actas as $a) {
            $alumno = Alumno::find($a['idAlumno']);
            $comision = Comision::find($a['idComision']);

            if ($alumno == null) {
                $error = ['success' => false, 'mensaje' => 'El alumno no existe'];
                return $error;
            }

            if ($comision == null) {
                $error = ['success' => false, 'mensaje' => 'La comisión no existe'];
                return $error;
            }

            $inscripcion = Inscripcion::esComision($a['idComision'])
                ->esAlumno($a['idAlumno'])->get()->first();


            if ($inscripcion->count() == 0) {
                $error = ['success' => false, 'mensaje' => 'Ha existido un error'];
                return $error;
            }

            $existeActa = Acta::tieneNota($inscripcion->id);
            if (!$existeActa->count() == 0) {
                $error = ['success' => false, 'mensaje' => 'Ha existido un error'];
                return $error;
            } else
                $acta = new Acta;


            if (!($a['nota'] <= 10 && $a['nota'] >= 0)) {
                $error = ['success' => false, 'mensaje' => 'Nota invalida.'];
                return $error;
            }

            $acta->nota = $a['nota'];
            $acta->id_inscripcion = $inscripcion->id;

            $acta->save();
        }


        $success = ['success' => true, 'mensaje' => 'Se han guardado las notas.'];

        return $success;
    }
}
