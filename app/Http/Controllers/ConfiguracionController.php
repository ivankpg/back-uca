<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Materia;
use App\Models\Facultad;
use App\Models\Carrera;
use App\Http\Resources\Facultad as FacultadResource;
use App\Http\Resources\Materia as MateriaResource;
use App\Http\Resources\Carrera as CarreraResource;

class ConfiguracionController extends Controller
{

    /**
     * Listado de materias
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getMaterias()
    {
        $materias = Materia::all();

        return MateriaResource::collection($materias);
    }

    /**
     * Listado de facultades
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getFacultades()
    {
        $comisiones = Facultad::all();

        return FacultadResource::collection($comisiones);
    }

    /**
     * Listado de carreras
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getCarreras()
    {
        $comisiones = Carrera::all();

        return CarreraResource::collection($comisiones);
    }
}
