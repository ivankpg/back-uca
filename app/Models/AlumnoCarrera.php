<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AlumnoCarrera extends Model
{
    protected $table = "alumnos_carreras";

    public function alumno(){
        return $this->belongsTo('App\Models\Alumno','id_alumno','id');
    }

    public function carrera(){
        return $this->hasOne('App\Models\Carrera','id','id_carrera');
    }

}
