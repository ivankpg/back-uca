<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Alumno extends Model
{
    protected $table = "alumnos";

    public function persona()
    {
        return $this->hasOne('App\Models\Persona','id','id_persona');
    }

    /*public function carreras(){
        return $this->hasManyThrough(
            'App\Models\Carrera',
            'App\Models\AlumnoCarrera',
            'id_alumno');
    }*/

    public function alumnoCarreras(){
        return $this->hasMany('App\Models\AlumnoCarrera', 'id_alumno', 'id');
    }

    public function inscripciones(){
        return $this->hasMany('App\Models\Inscripcion', 'id_alumno', 'id');
    }
}
