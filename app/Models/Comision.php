<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comision extends Model
{
    protected $table = "comisiones";

    public function facultad(){
        return $this->hasOne('App\Models\Facultad','id','id_facultad');
    }

    public function carrera(){
        return $this->hasOne('App\Models\Carrera','id','id_carrera');
    }

    public function materia(){
        return $this->hasOne('App\Models\Materia','id','id_materia');
    }

    public function inscripciones(){
        return $this->hasMany('App\Models\Inscripcion', 'id_comision', 'id');
    }


}
