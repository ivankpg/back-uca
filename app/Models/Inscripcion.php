<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Inscripcion extends Model
{
    protected $table = "inscripciones";

    public function alumnoCarrera(){
        return $this->belongsTo('App\Models\AlumnoCarrera','id_alumno','id');
    }

    public function comision(){
        return $this->belongsTo('App\Models\Comision', 'id_comision','id');
    }

    public function acta(){
        return $this->hasOne('App\Models\Acta','id_inscripcion','id');
    }


    public function scopeesComision($query, $comision)
    {
        return $query->where('id_comision', $comision);
    }


    public function scopeesAlumno($query, $alumno)
    {
        return $query->where('id_alumno', $alumno);
    }
}
