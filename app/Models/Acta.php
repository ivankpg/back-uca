<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Acta extends Model
{
    protected $table = "actas";

    public function inscripcion(){
        return $this->belongsTo('App\Models\Inscripcion');
    }

    public function scopetieneNota($query, $inscripcion)
    {
        return $query->where('id_inscripcion', $inscripcion);
    }

}
